import PropTypes from "prop-types";
import "./MovieItem.css";
import { useState } from "react";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  /*un hook State est créé, permettant de sauvegarder l'état 'favoris' 
  jusqu'à ce que la page soit sauvegardée.
  
  on créé donc une constant avec useState, qui permet de créer un tableau dans lequel se trouve
  une variable ainsi qu'une fonciton pour la modifier : 
  const [nomVariable, nomFoncitonPourModifierVariable] = useState(valeur par défaut)*/
  const [favorite, setFavorite] = useState(false);

  /* cette fonciton permet de stocker la valeur du useState précédent et
  de définir l'action du bouton cliquable
  ceci est une fonction fléchée, elle est anonyme (associée à une variable) et plus rapide à écrire, elle ne prend pas d'argument (parenthèse vide)
  setFavorite permet de modifier la variable favorite, et '!' indique l'inverse de la valeur de favorite
  sa valeur étant false, elle passe à vraie quand on clique sur le bouton, et inversement*/
  const toggleFavorite = () => {
    setFavorite(!favorite);
  }

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      <button type="button" onClick={toggleFavorite}>{favorite === true ? "retirer des favoris" : "ajouter aux favoris" }</button> 
    </div>
  );
}
/*Pour le bouton, on lui donne comme évènement OnClick toggleFavorite, qui inverse la valeur de favorite. Ensuite, avec un opérateur ternaire, 
on lui dit quoi afficher sur le bouton*/

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
